﻿namespace Kino
{
    partial class Klienci
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBTelefon = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TBMail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TBNazwisko = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TBImie = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Dodaj = new System.Windows.Forms.Button();
            this.TBPunkty = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TBTelefon
            // 
            this.TBTelefon.Location = new System.Drawing.Point(182, 137);
            this.TBTelefon.Name = "TBTelefon";
            this.TBTelefon.Size = new System.Drawing.Size(100, 20);
            this.TBTelefon.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(179, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Telefon";
            // 
            // TBMail
            // 
            this.TBMail.Location = new System.Drawing.Point(317, 65);
            this.TBMail.Name = "TBMail";
            this.TBMail.Size = new System.Drawing.Size(100, 20);
            this.TBMail.TabIndex = 20;
            this.TBMail.TextChanged += new System.EventHandler(this.TBMail_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(314, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "E-Mail";
            // 
            // TBNazwisko
            // 
            this.TBNazwisko.Location = new System.Drawing.Point(182, 65);
            this.TBNazwisko.Name = "TBNazwisko";
            this.TBNazwisko.Size = new System.Drawing.Size(100, 20);
            this.TBNazwisko.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(179, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Nazwisko";
            // 
            // TBImie
            // 
            this.TBImie.Location = new System.Drawing.Point(41, 65);
            this.TBImie.Name = "TBImie";
            this.TBImie.Size = new System.Drawing.Size(100, 20);
            this.TBImie.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Imię";
            // 
            // Dodaj
            // 
            this.Dodaj.Location = new System.Drawing.Point(41, 121);
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Size = new System.Drawing.Size(75, 37);
            this.Dodaj.TabIndex = 14;
            this.Dodaj.Text = "Dodaj";
            this.Dodaj.UseVisualStyleBackColor = true;
            this.Dodaj.Click += new System.EventHandler(this.Dodaj_Click);
            // 
            // TBPunkty
            // 
            this.TBPunkty.Location = new System.Drawing.Point(318, 137);
            this.TBPunkty.Name = "TBPunkty";
            this.TBPunkty.Size = new System.Drawing.Size(100, 20);
            this.TBPunkty.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(315, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Punkty";
            // 
            // Klienci
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 188);
            this.Controls.Add(this.TBPunkty);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TBTelefon);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TBMail);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TBNazwisko);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TBImie);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dodaj);
            this.Name = "Klienci";
            this.Text = "Klienci";
            this.Load += new System.EventHandler(this.Klienci_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TBTelefon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TBMail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBNazwisko;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBImie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Dodaj;
        private System.Windows.Forms.TextBox TBPunkty;
        private System.Windows.Forms.Label label5;
    }
}