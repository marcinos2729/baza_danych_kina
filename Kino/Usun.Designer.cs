﻿namespace Kino
{
    partial class Usun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Okno = new System.Windows.Forms.ListBox();
            this.TBImie = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TBNazwisko = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Szukaj = new System.Windows.Forms.Button();
            this.Skasuj = new System.Windows.Forms.Button();
            this.Zmien = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Okno
            // 
            this.Okno.FormattingEnabled = true;
            this.Okno.Location = new System.Drawing.Point(12, 12);
            this.Okno.Name = "Okno";
            this.Okno.Size = new System.Drawing.Size(278, 147);
            this.Okno.TabIndex = 2;
            // 
            // TBImie
            // 
            this.TBImie.Location = new System.Drawing.Point(296, 28);
            this.TBImie.Name = "TBImie";
            this.TBImie.Size = new System.Drawing.Size(100, 20);
            this.TBImie.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(293, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Imię";
            // 
            // TBNazwisko
            // 
            this.TBNazwisko.Location = new System.Drawing.Point(296, 89);
            this.TBNazwisko.Name = "TBNazwisko";
            this.TBNazwisko.Size = new System.Drawing.Size(100, 20);
            this.TBNazwisko.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(292, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nazwisko";
            // 
            // Szukaj
            // 
            this.Szukaj.Location = new System.Drawing.Point(296, 136);
            this.Szukaj.Name = "Szukaj";
            this.Szukaj.Size = new System.Drawing.Size(75, 23);
            this.Szukaj.TabIndex = 10;
            this.Szukaj.Text = "Szukaj";
            this.Szukaj.UseVisualStyleBackColor = true;
            this.Szukaj.Click += new System.EventHandler(this.Szukaj_Click);
            // 
            // Skasuj
            // 
            this.Skasuj.Enabled = false;
            this.Skasuj.Location = new System.Drawing.Point(27, 184);
            this.Skasuj.Name = "Skasuj";
            this.Skasuj.Size = new System.Drawing.Size(75, 23);
            this.Skasuj.TabIndex = 11;
            this.Skasuj.Text = "Usuń";
            this.Skasuj.UseVisualStyleBackColor = true;
            this.Skasuj.Click += new System.EventHandler(this.Skasuj_Click);
            // 
            // Zmien
            // 
            this.Zmien.Enabled = false;
            this.Zmien.Location = new System.Drawing.Point(126, 184);
            this.Zmien.Name = "Zmien";
            this.Zmien.Size = new System.Drawing.Size(75, 23);
            this.Zmien.TabIndex = 12;
            this.Zmien.Text = "Zmień";
            this.Zmien.UseVisualStyleBackColor = true;
            this.Zmien.Click += new System.EventHandler(this.Zmien_Click);
            // 
            // Usun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 232);
            this.Controls.Add(this.Zmien);
            this.Controls.Add(this.Skasuj);
            this.Controls.Add(this.Szukaj);
            this.Controls.Add(this.TBNazwisko);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TBImie);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Okno);
            this.Name = "Usun";
            this.Text = "Usuwanie Rezerwacji";
            this.Load += new System.EventHandler(this.Usun_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Okno;
        private System.Windows.Forms.TextBox TBImie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TBNazwisko;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Szukaj;
        private System.Windows.Forms.Button Skasuj;
        private System.Windows.Forms.Button Zmien;
    }
}