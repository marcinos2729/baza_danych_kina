﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace Kino
{
    public partial class Klienci : Form
    {
        public SqlDataAdapter Adapter;

        public Klienci()
        {
            InitializeComponent();
        }

        private void Dodaj_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TBImie.Text) && !string.IsNullOrEmpty(TBNazwisko.Text))
            {
                string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);

                Polaczenie.Open();

                Adapter = new SqlDataAdapter("SELECT * FROM Klienci", Polaczenie);
                SqlCommandBuilder Stworz = new SqlCommandBuilder(Adapter);

                DataSet Dane = new DataSet();
                Adapter.Fill(Dane, "Klienci");

                DataRow Wiersz = Dane.Tables["Klienci"].NewRow();

                Wiersz["IDKlienta"] = Dane.Tables["Klienci"].Rows.Count + 1;
                Wiersz["Imię"] = TBImie.Text;
                Wiersz["Nazwisko"] = TBNazwisko.Text;
                Wiersz["Mail"] = TBMail.Text;
                Wiersz["Telefon"] = TBTelefon.Text;
                Wiersz["Punkty"] = Int32.Parse(TBPunkty.Text);

                Dane.Tables["Klienci"].Rows.Add(Wiersz);

                Adapter.Update(Dane, "Klienci");

                Polaczenie.Close();
            }
            else { MessageBox.Show("Proszę uzupełnić Imię i Nazwisko Klienta"); };
        }

        private void Klienci_Load(object sender, EventArgs e)
        {

        }

        private void TBMail_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
