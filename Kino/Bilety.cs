﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Kino
{
    public partial class Bilety : Form
    {
        public SqlDataAdapter Adapter;

        public ArrayList ListaIndeksow = new ArrayList();

        public Bilety()
        {
            InitializeComponent();
            string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);
            Polaczenie.Open();
            SqlCommand Komenda = Polaczenie.CreateCommand();
            Komenda.CommandText = "SELECT IDFilmu, Tytuł FROM Filmy;";
            SqlDataReader Wczytanie = Komenda.ExecuteReader();

            ArrayList tmp = new ArrayList();
            while (Wczytanie.Read())
            {
                ListaIndeksow.Add(Wczytanie[0].ToString());
                tmp.Add(Wczytanie[1].ToString());
            }
            tmp.Sort();
            foreach(object o in tmp){
                LBTytul.Items.Add(o); 
            }

            Polaczenie.Close();
        }

        private void LBTytul_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);
            Polaczenie.Open();
            SqlCommand Komenda = Polaczenie.CreateCommand();
            Komenda.CommandText = "SELECT Data, Godzina, Sala FROM Seanse WHERE IDFilmu='" + Int32.Parse(ListaIndeksow[LBTytul.SelectedIndex].ToString()) + "';";
            SqlDataReader Wczytanie = Komenda.ExecuteReader();
            string Seans = "";
            string[] Data;
            string[] Godzina;
            LBSeans.Items.Clear();
            while (Wczytanie.Read())
            {
                Data = Wczytanie[0].ToString().Split();
                Godzina = Regex.Split(Wczytanie[1].ToString(), @"\D+");
                Seans = Data[0] + " " + Godzina[0] + ":" + Godzina[1] + " " + Wczytanie[2].ToString();
                LBSeans.Items.Add(Seans);
            }

            Polaczenie.Close();
        }

        private void LBSeans_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void Dodaj_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TBImie.Text) && !string.IsNullOrEmpty(TBNazwisko.Text))
            {
                string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);
                Polaczenie.Open();

                SqlCommand Komenda = Polaczenie.CreateCommand();
                Komenda.CommandText = "SELECT IDKlienta FROM Klienci WHERE Imię = '" + TBImie.Text + "' AND Nazwisko = '" + TBNazwisko.Text + "';";
                
                SqlDataReader Wczytanie = Komenda.ExecuteReader();

                int ID = 0;
                while (Wczytanie.Read())
                {
                    ID = Wczytanie.GetInt32(0);
                }
                Wczytanie.Close();

                int Seans = Int32.Parse(ListaIndeksow[LBTytul.SelectedIndex].ToString());

                Adapter = new SqlDataAdapter("SELECT * FROM Bilety", Polaczenie);
                SqlCommandBuilder Stworz = new SqlCommandBuilder(Adapter);
                DataSet Dane = new DataSet();
                Adapter.Fill(Dane, "Bilety");

                DataRow Wiersz = Dane.Tables["Bilety"].NewRow();

                Wiersz["IDKlienta"] = ID;
                Wiersz["IDSeansu"] = Seans;
                Wiersz["Typ"] = LBTyp.SelectedItem;
                if (Zaplacony.Checked == true) Wiersz["Zapłacony"] = "True";
                else Wiersz["Zapłacony"] = "False";

                Dane.Tables["Bilety"].Rows.Add(Wiersz);

                Adapter.Update(Dane, "Bilety");

                Polaczenie.Close();
            }
            else { MessageBox.Show("Proszę uzupełnić Imię i Nazwisko Klienta"); };
            
        }

        private void Bilety_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

    }
}
