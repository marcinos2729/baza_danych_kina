﻿namespace Kino
{
    partial class Kino
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Okno = new System.Windows.Forms.ListBox();
            this.Wszystkie = new System.Windows.Forms.Button();
            this.CheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.ListBox = new System.Windows.Forms.ListBox();
            this.Zapytanie = new System.Windows.Forms.Button();
            this.Przycisk_Filmy = new System.Windows.Forms.Button();
            this.Przycisk_Klienci = new System.Windows.Forms.Button();
            this.Rezerwuj = new System.Windows.Forms.Button();
            this.Usun = new System.Windows.Forms.Button();
            this.Przycisk_Seanse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Okno
            // 
            this.Okno.FormattingEnabled = true;
            this.Okno.Location = new System.Drawing.Point(68, 46);
            this.Okno.Name = "Okno";
            this.Okno.Size = new System.Drawing.Size(361, 186);
            this.Okno.TabIndex = 1;
            // 
            // Wszystkie
            // 
            this.Wszystkie.Enabled = false;
            this.Wszystkie.Location = new System.Drawing.Point(229, 362);
            this.Wszystkie.Name = "Wszystkie";
            this.Wszystkie.Size = new System.Drawing.Size(84, 23);
            this.Wszystkie.TabIndex = 2;
            this.Wszystkie.Text = "Wszystkie";
            this.Wszystkie.UseVisualStyleBackColor = true;
            this.Wszystkie.Click += new System.EventHandler(this.Wcisniecie);
            // 
            // CheckedListBox
            // 
            this.CheckedListBox.FormattingEnabled = true;
            this.CheckedListBox.Location = new System.Drawing.Point(210, 262);
            this.CheckedListBox.Name = "CheckedListBox";
            this.CheckedListBox.Size = new System.Drawing.Size(120, 94);
            this.CheckedListBox.TabIndex = 3;
            // 
            // ListBox
            // 
            this.ListBox.FormattingEnabled = true;
            this.ListBox.Items.AddRange(new object[] {
            "Filmy",
            "Klienci",
            "Seanse",
            "Bilety"});
            this.ListBox.Location = new System.Drawing.Point(68, 261);
            this.ListBox.Name = "ListBox";
            this.ListBox.Size = new System.Drawing.Size(120, 95);
            this.ListBox.TabIndex = 4;
            this.ListBox.SelectedIndexChanged += new System.EventHandler(this.ListBox_SelectedIndexChanged);
            // 
            // Zapytanie
            // 
            this.Zapytanie.Enabled = false;
            this.Zapytanie.Location = new System.Drawing.Point(354, 293);
            this.Zapytanie.Name = "Zapytanie";
            this.Zapytanie.Size = new System.Drawing.Size(75, 23);
            this.Zapytanie.TabIndex = 5;
            this.Zapytanie.Text = "Zapytanie";
            this.Zapytanie.UseVisualStyleBackColor = true;
            this.Zapytanie.Click += new System.EventHandler(this.Zapytanie_Click);
            // 
            // Przycisk_Filmy
            // 
            this.Przycisk_Filmy.Location = new System.Drawing.Point(455, 46);
            this.Przycisk_Filmy.Name = "Przycisk_Filmy";
            this.Przycisk_Filmy.Size = new System.Drawing.Size(84, 23);
            this.Przycisk_Filmy.TabIndex = 6;
            this.Przycisk_Filmy.Text = "Dodaj Film";
            this.Przycisk_Filmy.UseVisualStyleBackColor = true;
            this.Przycisk_Filmy.Click += new System.EventHandler(this.Przycisk_Filmy_Click);
            // 
            // Przycisk_Klienci
            // 
            this.Przycisk_Klienci.Location = new System.Drawing.Point(455, 79);
            this.Przycisk_Klienci.Name = "Przycisk_Klienci";
            this.Przycisk_Klienci.Size = new System.Drawing.Size(84, 23);
            this.Przycisk_Klienci.TabIndex = 7;
            this.Przycisk_Klienci.Text = "Dodaj Klienta";
            this.Przycisk_Klienci.UseVisualStyleBackColor = true;
            this.Przycisk_Klienci.Click += new System.EventHandler(this.Przycisk_Klienci_Click);
            // 
            // Rezerwuj
            // 
            this.Rezerwuj.Location = new System.Drawing.Point(455, 156);
            this.Rezerwuj.Name = "Rezerwuj";
            this.Rezerwuj.Size = new System.Drawing.Size(84, 23);
            this.Rezerwuj.TabIndex = 8;
            this.Rezerwuj.Text = "Rezerwuj";
            this.Rezerwuj.UseVisualStyleBackColor = true;
            this.Rezerwuj.Click += new System.EventHandler(this.Rezerwuj_Click);
            // 
            // Usun
            // 
            this.Usun.Location = new System.Drawing.Point(455, 195);
            this.Usun.Name = "Usun";
            this.Usun.Size = new System.Drawing.Size(84, 37);
            this.Usun.TabIndex = 9;
            this.Usun.Text = "Usuń Rezerwację";
            this.Usun.UseVisualStyleBackColor = true;
            this.Usun.Click += new System.EventHandler(this.Usun_Click);
            // 
            // Przycisk_Seanse
            // 
            this.Przycisk_Seanse.Location = new System.Drawing.Point(455, 113);
            this.Przycisk_Seanse.Name = "Przycisk_Seanse";
            this.Przycisk_Seanse.Size = new System.Drawing.Size(84, 23);
            this.Przycisk_Seanse.TabIndex = 10;
            this.Przycisk_Seanse.Text = "Dodaj Seans";
            this.Przycisk_Seanse.UseVisualStyleBackColor = true;
            this.Przycisk_Seanse.Click += new System.EventHandler(this.Przycisk_Seanse_Click);
            // 
            // Kino
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 403);
            this.Controls.Add(this.Przycisk_Seanse);
            this.Controls.Add(this.Usun);
            this.Controls.Add(this.Rezerwuj);
            this.Controls.Add(this.Przycisk_Klienci);
            this.Controls.Add(this.Przycisk_Filmy);
            this.Controls.Add(this.Zapytanie);
            this.Controls.Add(this.ListBox);
            this.Controls.Add(this.CheckedListBox);
            this.Controls.Add(this.Wszystkie);
            this.Controls.Add(this.Okno);
            this.Name = "Kino";
            this.Text = "Kino";
            this.Load += new System.EventHandler(this.Kino_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox Okno;
        private System.Windows.Forms.Button Wszystkie;
        private System.Windows.Forms.CheckedListBox CheckedListBox;
        private System.Windows.Forms.ListBox ListBox;
        private System.Windows.Forms.Button Zapytanie;
        private System.Windows.Forms.Button Przycisk_Filmy;
        private System.Windows.Forms.Button Przycisk_Klienci;
        private System.Windows.Forms.Button Rezerwuj;
        private System.Windows.Forms.Button Usun;
        private System.Windows.Forms.Button Przycisk_Seanse;

    }
}

