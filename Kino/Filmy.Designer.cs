﻿namespace Kino
{
    partial class Filmy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dodaj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TBTytul = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TBGatunek = new System.Windows.Forms.TextBox();
            this.TBRezyser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TBCzas = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CBNapisy = new System.Windows.Forms.CheckBox();
            this.CBLektor = new System.Windows.Forms.CheckBox();
            this.CBDubbing = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // Dodaj
            // 
            this.Dodaj.Location = new System.Drawing.Point(39, 162);
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Size = new System.Drawing.Size(75, 37);
            this.Dodaj.TabIndex = 0;
            this.Dodaj.Text = "Dodaj";
            this.Dodaj.UseVisualStyleBackColor = true;
            this.Dodaj.Click += new System.EventHandler(this.Dodaj_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tytuł";
            // 
            // TBTytul
            // 
            this.TBTytul.Location = new System.Drawing.Point(39, 56);
            this.TBTytul.Name = "TBTytul";
            this.TBTytul.Size = new System.Drawing.Size(100, 20);
            this.TBTytul.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(177, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Gatunek";
            // 
            // TBGatunek
            // 
            this.TBGatunek.Location = new System.Drawing.Point(180, 56);
            this.TBGatunek.Name = "TBGatunek";
            this.TBGatunek.Size = new System.Drawing.Size(100, 20);
            this.TBGatunek.TabIndex = 4;
            // 
            // TBRezyser
            // 
            this.TBRezyser.Location = new System.Drawing.Point(315, 56);
            this.TBRezyser.Name = "TBRezyser";
            this.TBRezyser.Size = new System.Drawing.Size(100, 20);
            this.TBRezyser.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(312, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Reżyser";
            // 
            // TBCzas
            // 
            this.TBCzas.Location = new System.Drawing.Point(456, 56);
            this.TBCzas.Name = "TBCzas";
            this.TBCzas.Size = new System.Drawing.Size(100, 20);
            this.TBCzas.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(453, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Czas Trwania";
            // 
            // CBNapisy
            // 
            this.CBNapisy.AutoSize = true;
            this.CBNapisy.Location = new System.Drawing.Point(39, 110);
            this.CBNapisy.Name = "CBNapisy";
            this.CBNapisy.Size = new System.Drawing.Size(58, 17);
            this.CBNapisy.TabIndex = 9;
            this.CBNapisy.Text = "Napisy";
            this.CBNapisy.UseVisualStyleBackColor = true;
            // 
            // CBLektor
            // 
            this.CBLektor.AutoSize = true;
            this.CBLektor.Location = new System.Drawing.Point(180, 110);
            this.CBLektor.Name = "CBLektor";
            this.CBLektor.Size = new System.Drawing.Size(56, 17);
            this.CBLektor.TabIndex = 11;
            this.CBLektor.Text = "Lektor";
            this.CBLektor.UseVisualStyleBackColor = true;
            // 
            // CBDubbing
            // 
            this.CBDubbing.AutoSize = true;
            this.CBDubbing.Location = new System.Drawing.Point(315, 110);
            this.CBDubbing.Name = "CBDubbing";
            this.CBDubbing.Size = new System.Drawing.Size(66, 17);
            this.CBDubbing.TabIndex = 13;
            this.CBDubbing.Text = "Dubbing";
            this.CBDubbing.UseVisualStyleBackColor = true;
            // 
            // Filmy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 228);
            this.Controls.Add(this.CBDubbing);
            this.Controls.Add(this.CBLektor);
            this.Controls.Add(this.CBNapisy);
            this.Controls.Add(this.TBCzas);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TBRezyser);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TBGatunek);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TBTytul);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dodaj);
            this.Name = "Filmy";
            this.Text = "Filmy";
            this.Load += new System.EventHandler(this.Filmy_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Dodaj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TBTytul;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBGatunek;
        private System.Windows.Forms.TextBox TBRezyser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBCzas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox CBNapisy;
        private System.Windows.Forms.CheckBox CBLektor;
        private System.Windows.Forms.CheckBox CBDubbing;
    }
}