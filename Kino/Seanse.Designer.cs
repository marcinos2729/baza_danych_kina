﻿namespace Kino
{
    partial class Seanse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBGodzina = new System.Windows.Forms.TextBox();
            this.TBData = new System.Windows.Forms.TextBox();
            this.LBTytul = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Dodaj = new System.Windows.Forms.Button();
            this.TBSala = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TBGodzina
            // 
            this.TBGodzina.Location = new System.Drawing.Point(160, 82);
            this.TBGodzina.Name = "TBGodzina";
            this.TBGodzina.Size = new System.Drawing.Size(100, 20);
            this.TBGodzina.TabIndex = 15;
            // 
            // TBData
            // 
            this.TBData.Location = new System.Drawing.Point(161, 37);
            this.TBData.Name = "TBData";
            this.TBData.Size = new System.Drawing.Size(100, 20);
            this.TBData.TabIndex = 14;
            // 
            // LBTytul
            // 
            this.LBTytul.FormattingEnabled = true;
            this.LBTytul.Location = new System.Drawing.Point(12, 37);
            this.LBTytul.Name = "LBTytul";
            this.LBTytul.Size = new System.Drawing.Size(130, 173);
            this.LBTytul.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Tytuł";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Godzina";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(157, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Data";
            // 
            // Dodaj
            // 
            this.Dodaj.Location = new System.Drawing.Point(160, 173);
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Size = new System.Drawing.Size(98, 37);
            this.Dodaj.TabIndex = 9;
            this.Dodaj.Text = "Dodaj";
            this.Dodaj.UseVisualStyleBackColor = true;
            this.Dodaj.Click += new System.EventHandler(this.Dodaj_Click);
            // 
            // TBSala
            // 
            this.TBSala.Location = new System.Drawing.Point(160, 129);
            this.TBSala.Name = "TBSala";
            this.TBSala.Size = new System.Drawing.Size(100, 20);
            this.TBSala.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(159, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Sala";
            // 
            // Seanse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 230);
            this.Controls.Add(this.TBSala);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TBGodzina);
            this.Controls.Add(this.TBData);
            this.Controls.Add(this.LBTytul);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dodaj);
            this.Name = "Seanse";
            this.Text = "Seanse";
            this.Load += new System.EventHandler(this.Seanse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TBGodzina;
        private System.Windows.Forms.TextBox TBData;
        private System.Windows.Forms.ListBox LBTytul;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Dodaj;
        private System.Windows.Forms.TextBox TBSala;
        private System.Windows.Forms.Label label4;
    }
}