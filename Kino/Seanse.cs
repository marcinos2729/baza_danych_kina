﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Data.SqlClient;

namespace Kino
{
    public partial class Seanse : Form
    {
        public SqlDataAdapter Adapter;

        public ArrayList ListaIndeksow = new ArrayList();

        public Seanse()
        {
            InitializeComponent();
            string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);
            Polaczenie.Open();
            SqlCommand Komenda = Polaczenie.CreateCommand();
            Komenda.CommandText = "SELECT IDFilmu, Tytuł FROM Filmy;";
            SqlDataReader Wczytanie = Komenda.ExecuteReader();

            while (Wczytanie.Read())
            {
                ListaIndeksow.Add(Wczytanie[0].ToString());
                LBTytul.Items.Add(Wczytanie[1].ToString());
            }

            Polaczenie.Close();
        }

        private void Dodaj_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TBData.Text) && !string.IsNullOrEmpty(TBGodzina.Text) && !string.IsNullOrEmpty(TBSala.Text))
            {
                string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);
                Polaczenie.Open();
                SqlCommand Komenda = Polaczenie.CreateCommand();

                Console.WriteLine(TBData.Text.Length + " " + TBGodzina.Text.Length);

                if (TBData.Text.Length < 11 && TBGodzina.Text.Length < 6)
                {
                    int ID = Int32.Parse(ListaIndeksow[LBTytul.SelectedIndex].ToString());

                    Adapter = new SqlDataAdapter("SELECT * FROM Seanse", Polaczenie);
                    SqlCommandBuilder Stworz = new SqlCommandBuilder(Adapter);
                    DataSet Dane = new DataSet();
                    Adapter.Fill(Dane, "Seanse");

                    DataRow Wiersz = Dane.Tables["Seanse"].NewRow();

                    Wiersz["IDFilmu"] = ID;
                    Wiersz["IDSeansu"] = Dane.Tables["Seanse"].Rows.Count + 1;
                    Wiersz["Data"] = TBData.Text;
                    Wiersz["Godzina"] = TBGodzina.Text;
                    Wiersz["Sala"] = TBSala.Text;

                    Dane.Tables["Seanse"].Rows.Add(Wiersz);

                    Adapter.Update(Dane, "Seanse");

                    Polaczenie.Close();

                }
                else { MessageBox.Show("Nieprawidłowa data lub godzina."); };

                Polaczenie.Close();
            }
            else { MessageBox.Show("Proszę uzupełnić pola."); };
        }

        private void Seanse_Load(object sender, EventArgs e)
        {

        }
    }
}
