﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Kino
{
    public partial class Usun : Form
    {
        public SqlDataAdapter Adapter;

        public Usun()
        {
            InitializeComponent();
        }

        private void Szukaj_Click(object sender, EventArgs e)
        {
            string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;MultipleActiveResultSets=true;AttachDbFilename=" + sciezka);
            Polaczenie.Open();
            Adapter = new SqlDataAdapter();
            SqlCommand Komenda = Polaczenie.CreateCommand();
            SqlCommand Komenda_2 = Polaczenie.CreateCommand();
            Komenda.CommandText = "SELECT IDKlienta FROM Klienci WHERE Imię='" + TBImie.Text + "' AND Nazwisko='" + TBNazwisko.Text + "';";
            Adapter.SelectCommand = Komenda;
            Okno.Items.Clear();

            SqlDataReader Wczytanie = null;
            Wczytanie = Komenda.ExecuteReader();

            int Indeks;
            if (Wczytanie.Read())
            {
                Indeks = Int32.Parse(Wczytanie[0].ToString());

                Komenda_2.CommandText = "SELECT * FROM Bilety WHERE IDKlienta='" + Indeks + "';";
                Adapter.SelectCommand = Komenda_2;
                SqlDataReader Wczytanie_2 = Komenda_2.ExecuteReader();

                int i = 4;
                string[] Kolumny = new string[i];
                string Wyswietl = "";

                while (Wczytanie_2.Read())
                {
                    for (int j = 0; j < i; j++)
                    {
                        Kolumny[j] = Wczytanie_2[j].ToString();
                        Wyswietl += Kolumny[j] + "  ";
                    }
                    Okno.Items.Add(Wyswietl);
                    Wyswietl = "";
                }
                Wczytanie_2.Close();

                if (Okno.Items.Count > 0)
                {
                    Skasuj.Enabled = true;
                    Zmien.Enabled = true;
                }
            }
            Polaczenie.Close();

        }

        private void Skasuj_Click(object sender, EventArgs e)
        {
            string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);
            Polaczenie.Open();
            string[] Kolumny = Okno.SelectedItem.ToString().Split();
            SqlCommand Komenda = Polaczenie.CreateCommand();
            Komenda.CommandText = "DELETE FROM Bilety WHERE IDKlienta='" + Kolumny[0] + "' AND IDSeansu='" + Kolumny[2] + "' AND Typ='" + Kolumny[4] + "';";
            MessageBox.Show(Komenda.CommandText);
            Komenda.Connection = Polaczenie;
            Komenda.ExecuteNonQuery();

            Polaczenie.Close();
        }

        private void Zmien_Click(object sender, EventArgs e)
        {
            string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);
            Polaczenie.Open();
            string[] Kolumny = Okno.SelectedItem.ToString().Split();
            SqlCommand Komenda = Polaczenie.CreateCommand();
            Komenda.CommandText = "UPDATE Bilety SET Zapłacony='True' WHERE IDKlienta='" + Kolumny[0] + "' AND IDSeansu='" + Kolumny[2] + "' AND Typ='" + Kolumny[4] + "';";
            MessageBox.Show(Komenda.CommandText);
            Komenda.Connection = Polaczenie;
            Komenda.ExecuteNonQuery();
            Polaczenie.Close();
        }

        private void Usun_Load(object sender, EventArgs e)
        {

        }
    }
}
