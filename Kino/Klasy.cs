﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kino
{
    class Film
    {
        public int IDFilmu;
        public string Tytul;
        public string Gatunek;
        public string Rezyser;
        public int Czas;
        public bool Napisy;
        public bool Lektor;
        public bool Dubbing;

        public Film() { }

        public Film(int WartoscA, string WartoscB, string WartoscC, string WartoscD, int WartoscE, bool LogicznaA, bool LogicznaB, bool LogicznaC) 
        {
            this.IDFilmu = WartoscA;
            this.Tytul = WartoscB;
            this.Gatunek = WartoscC;
            this.Rezyser = WartoscD;
            this.Czas = WartoscE;
            this.Napisy = LogicznaA;
            this.Lektor = LogicznaB;
            this.Dubbing = LogicznaC;
        }

        public Film(string Dane)
        {
            string[] Elementy = Dane.Split(' ');
            
            this.IDFilmu = Int32.Parse(Elementy[0]);
            this.Tytul = Elementy[1];
            this.Gatunek = Elementy[2];
            this.Rezyser = Elementy[3];
            this.Czas = Int32.Parse(Elementy[4]);

            if (Elementy[5] == "true") this.Napisy = true;
            else this.Napisy = false;
            if (Elementy[6] == "true") this.Lektor = true;
            else this.Lektor = false;
            if (Elementy[7] == "true") this.Dubbing = true;
            else this.Dubbing = false;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7}", IDFilmu, Tytul, Gatunek, Rezyser, Czas, Napisy, Lektor, Dubbing);
        }
    }


    class Klient
    {
        public int IDKlienta;
        public string Imie;
        public string Nazwisko;
        public string Adres;
        public string Telefon;
        public int Punkty;

        public Klient() { }

        public Klient(int WartoscA, string WartoscB, string WartoscC, string WartoscD, string WartoscE, int WartoscF)
        {
            this.IDKlienta = WartoscA;
            this.Imie = WartoscB;
            this.Nazwisko = WartoscC;
            this.Adres = WartoscD;
            this.Telefon = WartoscE;
            this.Punkty = WartoscF;
        }

        public Klient(string Dane)
        {
            string[] Elementy = Dane.Split(' ');

            this.IDKlienta = Int32.Parse(Elementy[0]);
            this.Imie = Elementy[1];
            this.Nazwisko = Elementy[2];
            this.Adres = Elementy[3];
            this.Telefon = Elementy[4];
            this.Punkty = Int32.Parse(Elementy[5]);
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5}", IDKlienta, Imie, Nazwisko, Adres, Telefon, Punkty);
        }
    }
}
