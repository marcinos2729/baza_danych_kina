﻿namespace Kino
{
    partial class Bilety
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dodaj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LBTytul = new System.Windows.Forms.ListBox();
            this.TBImie = new System.Windows.Forms.TextBox();
            this.TBNazwisko = new System.Windows.Forms.TextBox();
            this.LBTyp = new System.Windows.Forms.ListBox();
            this.Zaplacony = new System.Windows.Forms.CheckBox();
            this.LBSeans = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Dodaj
            // 
            this.Dodaj.Location = new System.Drawing.Point(12, 145);
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Size = new System.Drawing.Size(75, 23);
            this.Dodaj.TabIndex = 0;
            this.Dodaj.Text = "Dodaj";
            this.Dodaj.UseVisualStyleBackColor = true;
            this.Dodaj.Click += new System.EventHandler(this.Dodaj_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Imię";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nazwisko";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tytuł";
            // 
            // LBTytul
            // 
            this.LBTytul.FormattingEnabled = true;
            this.LBTytul.Location = new System.Drawing.Point(130, 31);
            this.LBTytul.Name = "LBTytul";
            this.LBTytul.Size = new System.Drawing.Size(120, 134);
            this.LBTytul.TabIndex = 4;
            this.LBTytul.SelectedIndexChanged += new System.EventHandler(this.LBTytul_SelectedIndexChanged);
            // 
            // TBImie
            // 
            this.TBImie.Location = new System.Drawing.Point(12, 31);
            this.TBImie.Name = "TBImie";
            this.TBImie.Size = new System.Drawing.Size(100, 20);
            this.TBImie.TabIndex = 5;
            // 
            // TBNazwisko
            // 
            this.TBNazwisko.Location = new System.Drawing.Point(12, 98);
            this.TBNazwisko.Name = "TBNazwisko";
            this.TBNazwisko.Size = new System.Drawing.Size(100, 20);
            this.TBNazwisko.TabIndex = 6;
            // 
            // LBTyp
            // 
            this.LBTyp.FormattingEnabled = true;
            this.LBTyp.Items.AddRange(new object[] {
            "Zwykły",
            "Ulgowy",
            "Oferta Specjalna"});
            this.LBTyp.Location = new System.Drawing.Point(130, 185);
            this.LBTyp.Name = "LBTyp";
            this.LBTyp.Size = new System.Drawing.Size(120, 30);
            this.LBTyp.TabIndex = 7;
            // 
            // Zaplacony
            // 
            this.Zaplacony.AutoSize = true;
            this.Zaplacony.Location = new System.Drawing.Point(130, 231);
            this.Zaplacony.Name = "Zaplacony";
            this.Zaplacony.Size = new System.Drawing.Size(78, 17);
            this.Zaplacony.TabIndex = 8;
            this.Zaplacony.Text = "Zapłacony";
            this.Zaplacony.UseVisualStyleBackColor = true;
            // 
            // LBSeans
            // 
            this.LBSeans.FormattingEnabled = true;
            this.LBSeans.Location = new System.Drawing.Point(267, 31);
            this.LBSeans.Name = "LBSeans";
            this.LBSeans.Size = new System.Drawing.Size(126, 186);
            this.LBSeans.TabIndex = 9;
            this.LBSeans.SelectedIndexChanged += new System.EventHandler(this.LBSeans_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(264, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Seans";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Bilety
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 260);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LBSeans);
            this.Controls.Add(this.Zaplacony);
            this.Controls.Add(this.LBTyp);
            this.Controls.Add(this.TBNazwisko);
            this.Controls.Add(this.TBImie);
            this.Controls.Add(this.LBTytul);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dodaj);
            this.Name = "Bilety";
            this.Text = "Bilety";
            this.Load += new System.EventHandler(this.Bilety_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Dodaj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox LBTytul;
        private System.Windows.Forms.TextBox TBImie;
        private System.Windows.Forms.TextBox TBNazwisko;
        private System.Windows.Forms.ListBox LBTyp;
        private System.Windows.Forms.CheckBox Zaplacony;
        private System.Windows.Forms.ListBox LBSeans;
        private System.Windows.Forms.Label label4;
    }
}