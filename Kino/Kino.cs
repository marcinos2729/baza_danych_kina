﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace Kino
{
    public partial class Kino : Form
    {
        public Kino()
        {
            InitializeComponent();
        }

        public SqlDataAdapter Adapter;

        private void WyswietlCalaTabele(String Nazwa_Tabeli)
        {
            DataSet Dane = new DataSet();
            Adapter.Fill(Dane, Nazwa_Tabeli);
            DataTable Tabela = Dane.Tables[0];

            foreach (DataRow Row in Tabela.Rows)
            {
                Okno.Items.Add(Row["Tytuł"] + " " + Row["Gatunek"]);
            }
        }

        private void Przycisk_Filmy_Click(object sender, EventArgs e)
        {
            Filmy Okno = new Filmy();
            Okno.Show();
        }

        private void Przycisk_Klienci_Click(object sender, EventArgs e)
        {
            Klienci Okno = new Klienci();
            Okno.Show();
        }

        private void Rezerwuj_Click(object sender, EventArgs e)
        {
            Bilety Okno = new Bilety();
            Okno.Show();
        }

        private void Usun_Click(object sender, EventArgs e)
        {
            Usun Okno = new Usun();
            Okno.Show();
        }

        private void Przycisk_Seanse_Click(object sender, EventArgs e)
        {
            Seanse Okno = new Seanse();
            Okno.Show();
        }

        private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckedListBox.Items.Clear();
            if (ListBox.SelectedIndex == 0)
            {
                CheckedListBox.Items.Add("Tytuł"); CheckedListBox.Items.Add("Gatunek"); CheckedListBox.Items.Add("Reżyser");
                CheckedListBox.Items.Add("Czas"); CheckedListBox.Items.Add("Napisy");
                CheckedListBox.Items.Add("Lektor"); CheckedListBox.Items.Add("Dubbing");
            }
            if (ListBox.SelectedIndex == 1)
            {
                CheckedListBox.Items.Add("Imię"); CheckedListBox.Items.Add("Nazwisko");
                CheckedListBox.Items.Add("Mail"); CheckedListBox.Items.Add("Telefon");
                CheckedListBox.Items.Add("Punkty");
            }
            if (ListBox.SelectedIndex == 2)
            {
                CheckedListBox.Items.Add("Data"); CheckedListBox.Items.Add("Godzina");
                CheckedListBox.Items.Add("Sala");
            }
            if (ListBox.SelectedIndex == 3)
            {
                CheckedListBox.Items.Add("Klient"); CheckedListBox.Items.Add("Film");
                CheckedListBox.Items.Add("Termin"); CheckedListBox.Items.Add("Sala");
                CheckedListBox.Items.Add("Typ");    CheckedListBox.Items.Add("Zapłacony");
            }
            Zapytanie.Enabled = true;
            Wszystkie.Enabled = true;
        }

        private void Wcisniecie(object sender, EventArgs e)
        {
            for (int i = 0; i < CheckedListBox.Items.Count; i++)
            {
                CheckedListBox.SetItemChecked(i, true);
            }
        }

        private void Zapytanie_Click(object sender, EventArgs e)
        {
            string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;MultipleActiveResultSets=true;AttachDbFilename=" + sciezka);
            Polaczenie.Open();
            Adapter = new SqlDataAdapter();
            SqlCommand Komenda = Polaczenie.CreateCommand();

            if (ListBox.SelectedIndex == 2)
            {
                ArrayList ListaIDFilmów = new ArrayList();
                ArrayList ListaTytułow = new ArrayList();

                Komenda.CommandText = "SELECT IDFilmu, Tytuł FROM Filmy;";
                SqlDataReader Wczytanie = Komenda.ExecuteReader();

                while (Wczytanie.Read())
                {
                    ListaIDFilmów.Add(Wczytanie[0].ToString());
                    ListaTytułow.Add(Wczytanie[1].ToString());
                }

                Wczytanie.Close();

                string Pytanie = "";
                int i = 0;

                for (i = 0; i < CheckedListBox.CheckedItems.Count; i++)
                {
                    Pytanie += CheckedListBox.CheckedItems[i] + ", ";
                }

                Pytanie = Pytanie.Substring(0, Pytanie.Length - 2);
                Komenda.CommandText = "SELECT " + "IDFilmu, " + Pytanie + " FROM " + ListBox.SelectedItem;
                Okno.Items.Clear();
                Wczytanie = Komenda.ExecuteReader();

                string[] Kolumny = new string[i+1];
                string Wyswietl = "";

                while (Wczytanie.Read())
                {
                    Kolumny[0] = ListaTytułow[ListaIDFilmów.IndexOf(Wczytanie[0].ToString())].ToString();
                    Wyswietl += Kolumny[0] + "  ";
                    for (int j = 1; j <= i; j++)
                    {
                        Kolumny[j] = Wczytanie[j].ToString();
                        Wyswietl += Kolumny[j] + "  ";
                    }
                    Okno.Items.Add(Wyswietl);
                    Wyswietl = "";
                }
            }
            else if(ListBox.SelectedIndex == 3) 
            {
                ArrayList ListaIDFilmów = new ArrayList();
                ArrayList ListaTytułow = new ArrayList();

                Komenda.CommandText = "SELECT IDFilmu, Tytuł FROM Filmy;";
                SqlDataReader Wczytanie = Komenda.ExecuteReader();
                while (Wczytanie.Read())
                {
                    ListaIDFilmów.Add(Wczytanie[0].ToString());
                    ListaTytułow.Add(Wczytanie[1].ToString());
                }
                Wczytanie.Close();

                string Pytanie = "";
                int ilosc = 0;
                int i = 0;
                bool[] Wybrane = {false, false, false, false};

                for (i = 0; i < CheckedListBox.CheckedItems.Count; i++)
                {
                    if (CheckedListBox.CheckedItems[i].ToString() == "Typ" || CheckedListBox.CheckedItems[i].ToString() == "Zapłacony")
                    {
                        Pytanie += CheckedListBox.CheckedItems[i] + ", ";
                        ilosc++;
                    }
                    else
                    {
                        if (CheckedListBox.CheckedItems[i].ToString() == "Klient")
                            Wybrane[0] = true;
                        else if (CheckedListBox.CheckedItems[i].ToString() == "Film")
                            Wybrane[1] = true;
                        else if (CheckedListBox.CheckedItems[i].ToString() == "Termin")
                            Wybrane[2] = true;
                        else if (CheckedListBox.CheckedItems[i].ToString() == "Sala")
                            Wybrane[3] = true;
                    }
                }

                Pytanie = Pytanie.Substring(0, Pytanie.Length - 2);
                Komenda.CommandText = "SELECT " + "IDKlienta, IDSeansu, " + Pytanie + " FROM " + ListBox.SelectedItem;
                Okno.Items.Clear();
                Wczytanie = Komenda.ExecuteReader();

                string[] Kolumny = new string[i+2];
                string Wyswietl = "";
                int indeks = 0;
                SqlCommand Komenda_2 = Polaczenie.CreateCommand();

                while (Wczytanie.Read())
                {
                    indeks = -1;
                    if (Wybrane[0] == true)
                    {
                        Komenda_2.CommandText = "SELECT Imię, Nazwisko FROM Klienci WHERE IDKlienta = " + Wczytanie[0].ToString();
                        SqlDataReader Wczytanie_2 = Komenda_2.ExecuteReader();
                        while (Wczytanie_2.Read())
                        {
                            Kolumny[++indeks] = Wczytanie_2[0].ToString();
                            Kolumny[++indeks] = Wczytanie_2[1].ToString();
                        }
                        Wczytanie_2.Close();
                    }

                    if (Wybrane[1] == true)
                    {
                        Komenda_2.CommandText = "SELECT IDFilmu FROM Seanse WHERE IDSeansu = " + Wczytanie[1].ToString();
                        SqlDataReader Wczytanie_2 = Komenda_2.ExecuteReader();
                        while (Wczytanie_2.Read())
                        {
                            Kolumny[++indeks] = ListaTytułow[ListaIDFilmów.IndexOf(Wczytanie_2[0].ToString())].ToString();
                        }
                        Wczytanie_2.Close();
                    }

                    if (Wybrane[2] == true)
                    {
                        Komenda_2.CommandText = "SELECT Data, Godzina FROM Seanse WHERE IDSeansu = " + Wczytanie[1].ToString();
                        SqlDataReader Wczytanie_2 = Komenda_2.ExecuteReader();
                        while (Wczytanie_2.Read())
                        {
                            Kolumny[++indeks] = Wczytanie_2[0].ToString();
                            Kolumny[++indeks] = Wczytanie_2[1].ToString();
                        }
                        Wczytanie_2.Close();
                    }

                    if (Wybrane[3] == true)
                    {
                        Komenda_2.CommandText = "SELECT Sala FROM Seanse WHERE IDSeansu = " + Wczytanie[1].ToString();
                        SqlDataReader Wczytanie_2 = Komenda_2.ExecuteReader();
                        while (Wczytanie_2.Read())
                        {
                            Kolumny[++indeks] = Wczytanie_2[0].ToString();
                        }
                        Wczytanie_2.Close();
                    }

                    if (indeks != -1)
                    {
                        for (int j = 0; j <= indeks; j++)
                        {
                            Wyswietl += Kolumny[j] + "  ";
                        }
                    }

                    for (int k = 2; k <= ilosc+1; k++)
                    {
                        Kolumny[k + (indeks - 1)] = Wczytanie[k].ToString();
                        Wyswietl += Kolumny[k + (indeks - 1)] + "  ";
                    }
                    Okno.Items.Add(Wyswietl);
                    Wyswietl = "";
                }
            }
            else if (ListBox.SelectedIndex == 0 || ListBox.SelectedIndex == 1)
            {
                string Pytanie = "";
                int i = 0;
                for (i = 0; i < CheckedListBox.CheckedItems.Count; i++)
                {
                    Pytanie += CheckedListBox.CheckedItems[i] + ", ";
                }
                Pytanie = Pytanie.Substring(0, Pytanie.Length - 2);
                Komenda.CommandText = "SELECT " + Pytanie + " FROM " + ListBox.SelectedItem;
                Adapter.SelectCommand = Komenda;
                Okno.Items.Clear();

                SqlDataReader Wczytanie = null;
                Wczytanie = Komenda.ExecuteReader();

                string[] Kolumny = new string[i];
                string Wyswietl = "";

                while (Wczytanie.Read())
                {
                    for (int j = 0; j < i; j++)
                    {
                        Kolumny[j] = Wczytanie[j].ToString();
                        Wyswietl += Kolumny[j] + "  ";
                    }
                    Okno.Items.Add(Wyswietl);
                    Wyswietl = "";
                }
            }
            Polaczenie.Close();

        }

        private void Kino_Load(object sender, EventArgs e)
        {

        }
    }
}
