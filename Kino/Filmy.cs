﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;


namespace Kino
{
    public partial class Filmy : Form
    {
        public SqlDataAdapter Adapter;

        public Filmy()
        {
            InitializeComponent();
        }

        private void Dodaj_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TBTytul.Text) && !string.IsNullOrEmpty(TBGatunek.Text) && !string.IsNullOrEmpty(TBRezyser.Text))
            {
                string sciezka = @"C:\Users\Marcin\Desktop\Kino\Kino\Baza.mdf";
                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;AttachDbFilename=" + sciezka);

                Polaczenie.Open();

                Adapter = new SqlDataAdapter("SELECT * FROM Filmy", Polaczenie);
                SqlCommandBuilder Stworz = new SqlCommandBuilder(Adapter);

                DataSet Dane = new DataSet();
                Adapter.Fill(Dane, "Filmy");

                DataRow Wiersz = Dane.Tables["Filmy"].NewRow();

                Wiersz["IDFilmu"] = Dane.Tables["Filmy"].Rows.Count + 1;
                Wiersz["Tytuł"] = TBTytul.Text;
                Wiersz["Gatunek"] = TBGatunek.Text;
                Wiersz["Reżyser"] = TBRezyser.Text;
                Wiersz["Czas"] = Int32.Parse(TBCzas.Text);
                if (CBNapisy.Checked == true) Wiersz["Napisy"] = "True";
                else Wiersz["Napisy"] = "False";
                if (CBLektor.Checked == true) Wiersz["Lektor"] = "True";
                else Wiersz["Lektor"] = "False";
                if (CBDubbing.Checked == true) Wiersz["Dubbing"] = "True";
                else Wiersz["Dubbing"] = "False";

                Dane.Tables["Filmy"].Rows.Add(Wiersz);

                Adapter.Update(Dane, "Filmy");

                Polaczenie.Close();
            }
            else { MessageBox.Show("Proszę uzupełnić Tytuł, Gatunek i Imię Reżysera"); };
        }

        private void Filmy_Load(object sender, EventArgs e)
        {

        }


    }
}
